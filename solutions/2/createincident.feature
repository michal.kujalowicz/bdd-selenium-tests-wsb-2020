Feature: creating an incident

  Scenario:
    Given icm app is working
    When we
    Then

  Scenario: Tworzenie incydentu
    Given user is logged in
    When user clicks button Create Incident
    Then User can create incident


  Scenario:
    Given: Aplikacja uruchomiona i zalogowany
    And: Button jest widoczny
    When: User fills all fields
    And: Click create new incident
    Then: Incident is created
    And: incident can be viewed

  Scenario:
    Given: Za duzo danych
    And: Button jest widoczny
    When: Too long description
    Then: Error
    And: Incident not created

  Scenario: User does not provide Description
    Given:
    When: User does not fill Description
    Then: Incident cannot be created
    And: Error message is shown


