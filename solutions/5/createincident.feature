Feature: creating an incident

  Scenario: logged in user sees create incident button
    Given icm app is working
    When user logs in
    Then create incident button is visible

  Scenario: completing new incident dialog creates an incident
    Given icm app is working
    And user is logged in
    When user fills all fields on new incident dialog
    Then incident is created
    And incident can be viewed


  Scenario Outline: not full new incident dialog fails incident creation
    Given icm app is working
    And user is logged in
    When user does not fill <field> on new incident dialog
    Then incident is not created

    Examples:
    | field |
    | type |
    | description |
    | addressLine |